import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { StudentComponent } from './user/student/student.component';
import { TeacherComponent } from './user/teacher/teacher.component';
import { PrincipalComponent } from './user/principal/principal.component';
import { StudentNewComponent } from './user/student/student-new/student-new.component';
import { StudentDetailComponent } from './user/student/student-detail/student-detail.component';
import { RouterModule } from '@angular/router';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';

import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    StudentComponent,
    TeacherComponent,
    PrincipalComponent,
    StudentNewComponent,
    StudentDetailComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule, RouterModule.forRoot([
      {path: 'student', component: StudentComponent},
      {path: 'teacher', component: TeacherComponent},
      {path: 'principal', component: PrincipalComponent}
    ]), MatButtonModule, BrowserAnimationsModule, MatInputModule, HttpModule, MatSelectModule,MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
