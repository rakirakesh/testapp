export const UserDetails = [
    {
        stdName: '6th',
        sections: [
{
                sectionName: 'A',
                students: [
                    {
                        firstName: 'Navneet',
                        lastName: 'Kunal'
                    },
                    {
                        firstName: 'Runit',
                        lastName: 'Raushan'
                    }
                ]
            },
            {
                sectionName: 'B',
                students: [
                    {
                        firstName: 'Rakesh',
                        lastName: 'Kumar'
                    },
                    {
                        firstName: 'Nagendra',
                        lastName: 'Kumar'
                    }
                ]
            },
        ]
    },
    {
        stdName: '7th',
        sections: [
            {
                sectionName: 'A',
                students: [
                    {
                        firstName: 'Ramesh',
                        lastName: 'Kumar'
                    },
                    {
                        firstName: 'Saket',
                        lastName: 'Kumar'
                    }
                ]
            },
            {
                sectionName: 'B',
                students: [
                    {
                        firstName: 'Manish',
                        lastName: 'Kumar'
                    },
                    {
                        firstName: 'Priyanka',
                        lastName: 'Kumari'
                    }
                ]
            },
            {
                sectionName: 'C',
                students: [
                    {
                        firstName: 'Chanki',
                        lastName: 'Pandey'
                    },
                    {
                        firstName: 'Murli',
                        lastName: 'Karthik'
                    }
                ]
            }
        ]
    },
    {
        stdName: '8th',
        sections: [
            {
                sectionName: 'A',
                students: [
                    {
                        firstName: 'Navneet',
                        lastName: 'Kunal'
                    },
                    {
                        firstName: 'Runit',
                        lastName: 'Raushan'
                    }
                ]
            },
            {
                sectionName: 'B',
                students: [
                    {
                        firstName: 'Navneet',
                        lastName: 'Kunal'
                    },
                    {
                        firstName: 'Runit',
                        lastName: 'Raushan'
                    }
                ]
            },
        ]
    } 
];