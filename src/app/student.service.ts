import { Injectable } from '@angular/core';
import { UserDetails } from './constants/user-detail'


@Injectable()
export class StudentService {

    studentDetail: any [] = UserDetails;

    getStudentInfo(){
        return this.studentDetail;
    }
    
} 