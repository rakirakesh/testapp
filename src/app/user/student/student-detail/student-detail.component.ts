import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { StudentDetail } from '../student-model';


@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.css']
})
export class StudentDetailComponent{

  @Input() studentNewDetails : any;
  @Output() deleteStudentInfo: EventEmitter <any> = new EventEmitter();

  ngOnInit(){
    
  }

  onDelete(i){
    this.deleteStudentInfo.emit(i);
  }

 /*  @Input() studentName: string;
  @Input() studentClass: string;
  @Input() studentSchoolName: string;
  @Input() i: number;

  @Output() deleteStudentDetails: EventEmitter <any> = new EventEmitter();

  constructor() { 
    
  }

  deleteInfo(){
    this.deleteStudentDetails.emit(this.i);
  }
 */
}
