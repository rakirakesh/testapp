import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { StudentDetail } from '../student-model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-student-new',
  templateUrl: './student-new.component.html',
  styleUrls: ['./student-new.component.css'],
})
export class StudentNewComponent {

  @Input() addNewStudentDetails : StudentDetail;
  @Output() sendStudentDetails: EventEmitter <any> = new EventEmitter();


  constructor(){

  }

  addToList(fn,ln){
    let newStudent: StudentDetail = {
      firstName: fn,
      lastName: ln
    };
    this.sendStudentDetails.emit(newStudent);
  }


  /* @Output() sendStudentDetails: EventEmitter <any> = new EventEmitter();

  studentForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) {}
  
  ngOnInit() {
    this.studentForm = this.formBuilder.group({
      name: [null, [Validators.required, Validators.pattern("[a-zA-Z][a-zA-Z ]+"), Validators.minLength(2), Validators.maxLength(10)]],
      standard: [null, [Validators.required, Validators.pattern("[0-9]*")]],
      school: [null, Validators.required]
    });
  }

  public add(){
    let selectedDetails = new StudentDetail (this.studentForm.value.name, this.studentForm.value.standard, this.studentForm.value.school);
    console.log(selectedDetails);
    this.sendStudentDetails.emit(selectedDetails);
  }
 */
}
