import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StudentDetail } from './student-model';
import { StudentService } from 'src/app/student.service';



@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css'],
  providers: [StudentService]
})
export class StudentComponent implements OnInit{

  studentDetail : any = [];
  secName =[];
  studentInfo =[];
  showInfo = true;
  addInfo = true;
  @Output() addNewStudentDetails: EventEmitter <any> = new EventEmitter();

  constructor(private _studentDetail: StudentService){}

  ngOnInit() {

    this.studentDetail= this._studentDetail.getStudentInfo();

  }

  onClassChange(className){
    let sectionName= className.value.sections;
    this.secName= sectionName;
  }

  onSecChange(item){
    let studentName = item.value.students;
    this.studentInfo = studentName;
    this.showInfo = false;
  }

  addNewStudent(){
    this.addNewStudentDetails.emit(this.studentInfo);
    this.addInfo=false;
  }

  newStudentInfo(item){
    this.studentInfo.push(item);
  }

  deleteStudentInfo(index){
    this.studentInfo.splice(index,1);
  }

  /* student : any = [];
  detail: StudentDetail [];
  
  addDetails = false;
  viewDetails = false;

  constructor(private _studentDetail: StudentService) { 
    this.detail = [
      new StudentDetail ('Mark', 9, 'Oxford'),
      new StudentDetail ('Jacob', 10, 'IIT'),
      new StudentDetail ('Lary', 9, 'Cambridge')
    ];
  }  

  public getStudentDetails(data: any) {
      this.detail.push(data);
  }

  public deleteStudentInfo(index : number){
    debugger;
    this.detail.splice(index, 1);
  }


   toAdd(){
    this.addDetails= !this.addDetails;
  }

  toView(){
    this.viewDetails= !this.viewDetails;
  }
 */
} 
